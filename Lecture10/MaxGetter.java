public class MaxGetter
{
    public static int max(int x, int y, int z)
    {
        
        int result = x;
        if (y > result)
        {
            result = y;
        }
        if (z > result)
        {
            result = z;
        }
        
        return result;
    }
}