/*Top: Dice library contains multiple dice rolling methods*/

public class Dice
{
    public static int rollD6()
    {
        int die = (int)(Math.random()*6) + 1;
        return die;
    }
}