/*
Top: Solve a mathematical expression
Author: Ted Holmberg
*/

import java.util.Scanner;

public class Switch2If
{
    public static void main(String[] args)
    {
        // declare variable for the lefthand Operand
        int lefthand;
        // declare variable for the righthand Operand
        int righthand;
        // declare variable for the operator
        String operator;
        // setup a scanner object for input
        Scanner input = new Scanner(System.in);
        //setup a variable to hold result
        int result;
        
        // prompt the user for math expression
        System.out.print("Enter a mathematical expression seperated by spaces: ");
        // get the lefthand operand from user
        lefthand = input.nextInt();
        // get the operator from the user
        operator = input.next();
        // get the righthand operand from user
        righthand = input.nextInt();

        
        // multiselection statement where:
        // if operator is a + we add
        if ( operator.equals("+") )
        {
            result = lefthand + righthand;
            System.out.printf("%d%s%d=%d\n",lefthand,operator,righthand,result);
        }
        // if operator is a - we subtract
        else if ( operator.equals( "-") )
        {
            result = lefthand - righthand;
            System.out.printf("%d%s%d=%d\n",lefthand,operator,righthand,result);
            
        }
        // if operator is a * we multiply
        else if ( operator.equals("x") || operator.equals("X") || operator.equals("*") )
        {
            result = lefthand * righthand;
            System.out.printf("%d%s%d=%d\n",lefthand,operator,righthand,result);
        }
        // if operator is a / we divide
        else if ( operator.equals("/") )
        {
            result = lefthand / righthand;
            System.out.printf("%d%s%d=%d\n",lefthand,operator,righthand,result);
        }
        else
        {
            System.out.printf("%s is not a valid operator\n", operator);
        }
        
    }
}