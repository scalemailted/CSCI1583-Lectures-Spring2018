public class Enemy
{
    private static String name;
    
    public static void start(String name)
    {
        Enemy.name = name;
    }
    
    public static String getName()
    {
        return name;
    }
}