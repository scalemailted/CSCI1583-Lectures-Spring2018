import java.util.ArrayList;

public class ArrayListTester
{
    
    public static void main(String[] args)
    {
        ArrayList<String> items = new ArrayList<String>();
        displayArray(items);
        items.add("red");
        displayArray(items);
        items.add(0, "yellow");
        displayArray(items);
        items.add(0, "blue");
        displayArray(items);
        items.add(2, "green");
        displayArray(items);
        items.add("yellow");
        displayArray(items);
        
        System.out.printf("The element at index 1 is: %s\n", items.get(1) );
        
        boolean bool = items.contains("blue");
        System.out.printf("items contains blue? %b\n", bool);
        
        items.remove("blue");
        
        bool = items.contains("blue");
        System.out.printf("items contains blue? %b\n", bool);
        displayArray(items);
        
        System.out.println(items);
        System.out.println(items.toArray());
        
        for(int i=0; i<items.size(); i++)
        {
            String color = items.get(i);
            System.out.println(color);
        }
        
        if (items.contains("red") == true)
        {
            int indexRed = items.indexOf("red");
            items.remove("red");
            items.add(indexRed, "purple");
        }
        
        ArrayList<ArrayList<String>> arrArrItems = new ArrayList<ArrayList<String>>();
        arrArrItems.add(new ArrayList<String>());
        arrArrItems.get(0).add("Hello");
        System.out.println(arrArrItems);
        
        ArrayList<String[]> things = new ArrayList<String[]>();
        String[] stuff = {"Hello", "World", "BuyBye"};
        things.add(stuff);
        System.out.println(things);
    }
    
    public static void displayArray(ArrayList<String> items)
    {
        System.out.println("size: " + items.size() + " " + items.toString() );
    }
}