/*
Author: Ted Holmberg
Top:
Determine a student's average for ANY number of quizzes*/

import java.util.Scanner;

public class SentinelAverager
{
    public static void main(String[] args)
    {
        // Initialize sum to 0
        double sum = 0;
        // Initilize count to 0
        int count = 0;
        // Init Scanner object for input
        Scanner input = new Scanner(System.in);
        // Declare a average
        double average;
        // Declare grade
        double grade;
        
        // Prompt the user for a grade
        System.out.print("Enter a grade: ");
        // Get and store the grade from user
        grade = input.nextDouble();
        
        // Repeat until grade is less than 0
        while (grade >= 0)
        {
            //add grade to the sum
            sum += grade;
            // add one to the count
            count++;
            // Prompt the user for a grade
             System.out.print("Enter a grade: ");
            // Get and store the grade from user
            grade = input.nextDouble();
        }
        
        // Set the average to sum / count
        average = sum / count;
        // print average
        System.out.printf("Average: %.2f\n", average);
    }
}