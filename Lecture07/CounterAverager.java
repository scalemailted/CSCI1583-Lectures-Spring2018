/*
Author: Ted Holmberg
Top: Determine a student's average for 10 quizzes.
*/


import java.util.Scanner;

public class CounterAverager
{
    public static void main(String[] args)
    {
        // Initialize sum to 0
        double sum = 0.0;
        // Initialize count to 0
        int count = 0;
        // Initialize Scanner object for input
        Scanner input = new Scanner(System.in);
        // Declare an average
        double average;
        
        // repeat until count is 10
        //for(int count = 0; count < 10; count++) 
        while (count < 10)
        {
            //Prompt the user for a grade
            System.out.print("Enter a grade: ");
            //Get and store the grade from the user
            double grade = input.nextDouble();
            //Add grade to the sum
            sum += grade; //i.e. sum = sum + grade
            //add one to the count
            count++; //i.e. count = count + 1;
        }
        
        // Set the average to the sum / count
        average = sum / count;
        // Print average
        System.out.printf("Average %.2f\n", average);
    }
}