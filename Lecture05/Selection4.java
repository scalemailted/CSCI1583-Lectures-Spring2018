//broken multiselection (doesn't use the elses)

import java.util.Scanner;

public class Selection4
{
    public static void main(String[] args)
    {
        //Prompt user for a numerical grade
        System.out.print("Enter a numerical grade: ");
        //Get a numerical grade and save it
        Scanner keyboard = new Scanner(System.in);
        double grade = keyboard.nextDouble();
        //Determine if grade is an A
        if (grade >= 90.0)
        {
            System.out.print("You got an A\n");
        }
        //otherwise check for B
        if (grade >= 80)
        {
            System.out.print("You got a B\n");
        }
        //otherwise check for C
        if (grade >= 70)
        {
            System.out.print("You got a C\n");
        }
        //otherwise check for D
        if (grade >= 60)
        {
            System.out.print("You got a D\n");
        }
        //others its an F
        else
        {
            System.out.print("You got an F\n");
        }
        
        
        System.out.println("Thanks for using this app.");    
        
    }
}