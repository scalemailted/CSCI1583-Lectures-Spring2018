//Single selection

import java.util.Scanner;

public class Selection1
{
    public static void main(String[] args)
    {
        //Prompt user for a numerical grade
        System.out.print("Enter a numerical grade: ");
        //Get a numerical grade and save it
        Scanner keyboard = new Scanner(System.in);
        double grade = keyboard.nextDouble();
        //Determine if grade is passing
        if (grade >= 70.0)
        {
            //True: Print that you passed.
            System.out.println("You passed!");
        }
        
        System.out.println("Thanks for using this app.");    
        
    }
}