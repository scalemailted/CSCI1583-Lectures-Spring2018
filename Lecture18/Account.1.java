public class Account
{
    private double balance;
    
    public Account()
    {
        this.balance = 0.0; 
    }
    
    public double getBalance()
    {
        return this.balance;
    }
}