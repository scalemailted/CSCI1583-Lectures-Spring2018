public class Account
{
    private static int count = 0;
    
    private String name;
    private String pin;
    private int id;
    private double balance;
    
    public Account(String name, String pin)
    {
        Account.count++;
        this.name = name;
        this.pin = pin;
        this.id = Account.count;
        this.balance = 0.0; 
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public int getID()
    {
        return this.id;
    }
    
    public double getBalance()
    {
        return this.balance;
    }
    
    public boolean equalsPin(String attempt)
    {
        boolean valid = attempt.equals(this.pin);
        return valid;
    }
    
    public String toString()
    {
        return String.format("Name: %s, Acct ID: %d, Balance: $%.2f",
                            this.name, 
                            this.id,
                            this.balance);
    }
}