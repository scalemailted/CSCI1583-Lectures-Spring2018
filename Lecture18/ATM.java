public class ATM
{
    public static void main(String[] args)
    {
        Account account1 = new Account("Joe Joey", "0060");
        Account account2 = new Account("Sue Suzie", "1234");
        System.out.println(account1.getName());
        System.out.println(account1.getID());
        System.out.println(account1.getBalance());
        System.out.println(account1.equalsPin("0060") );
        
        System.out.println(account2.getName());
        System.out.println(account2.getID());
        System.out.println(account2.getBalance());
        System.out.println(account2.equalsPin("0060") );
        
        System.out.println(account1.toString() );
        
        
        
    }
}