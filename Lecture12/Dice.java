/*Top: Dice library contains multiple dice rolling methods*/

public class Dice
{
    public static int rollD6()
    {
        int die = (int)(Math.random()*6) + 1;
        return die;
    }
    
    public static int rollD6( int quantity)
    {
        String comboText = "";
        int dieSum = 0;
        
        for(int i=0; i < quantity; i++)
        {
            int die = rollD6();
            dieSum += die;
            comboText += die;
            if (i != quantity-1)
            {
                comboText += "+";
            }
            else
            {
                comboText += "=";
            }
        }
        comboText += dieSum;
        System.out.println(comboText);
        return dieSum;
    }
}