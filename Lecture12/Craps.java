/*
Top:  The Game of Craps
Name: Ted Holmberg
*/

public class Craps
{
    /* Setup data */
    
    //Create new datatype to track status
    private enum Status {CONTINUE, LOSE, WIN};
    
    //Constants used for Game rules
    private static final int SNAKE_EYES = 2;
    private static final int TREY = 3;
    private static final int SEVEN = 7;
    private static final int YO = 11;
    private static final int BOXCAR = 12;
    
    //Variables to track the game status and my point
    private static Status gameStatus;
    private static int myPoint;
    
    
    public static void main(String[] args)
    {
        // Play the first round
        firstRound();
        // While there is no winner or loser then conitinue playing next rounds
        while (gameStatus == Status.CONTINUE)
        {
            nextRound();
        }
        // Print game results
        printResults();
    }
    
    //First Round:
    public static void firstRound()
    {
        // roll 2 dice
        int dieSum = Dice.rollD6(2);
        // if roll is SEVEN or YO
        if (dieSum == SEVEN || dieSum == YO)
        {
            //update game status to WIN
            gameStatus = Status.WIN;
        }
        // else if roll is SNAKE_EYES or TREY or BOXCARS
        else if (dieSum == SNAKE_EYES || dieSum == TREY || dieSum == BOXCAR)
        {
            //update game status to LOSE
            gameStatus = Status.LOSE;
        }
        // else
        else
        {
            //update game status to continue 
            gameStatus = Status.CONTINUE;
            //remember myPoint
            myPoint = dieSum;
            //next Round
            nextRound();
        }
    }
    
    // Next Round:
    public static void nextRound()
    {
        //     roll dice
        int dieSum = Dice.rollD6(2);
        //     if dice roll is myPoint update the game status to WIN
        if (dieSum == myPoint)
        {
            gameStatus = Status.WIN;
        }
        //     else if dice roll is SEVEN update the game status to LOST
        else if (dieSum == SEVEN)
        {
            gameStatus = Status.LOSE;
        }
    }
     
    // Print Results:   
    public static void printResults()
    {
        // if game status is WIN we print victory message
        if (gameStatus == Status.WIN)
        {
            System.out.print("You won!\n");
        }
        // else we print a LOST message
        else if (gameStatus == Status.LOSE)
        {
            System.out.print("You Lose!\n");
        }
    }
}