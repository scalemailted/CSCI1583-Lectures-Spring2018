import java.util.Scanner;

public class InputOperation
{
    public static void main(String[] args)
    {
        //Create a Scanner object for System.in i.e. the Keyboard
        Scanner input = new Scanner(System.in);
        //Ask Scanner object for the next text data
        System.out.print("Enter your name: ");
        String text = input.nextLine();
        System.out.printf("Hello %s\n", text);
        
        System.out.print("Enter the year: ");
        int year = input.nextInt();
        System.out.printf("You are at %d\n", year);
        
        System.out.print("Enter some dollar amount: ");
        double dollars = input.nextDouble();
        System.out.printf("You have $%.2f\n", dollars);
        
    }
}