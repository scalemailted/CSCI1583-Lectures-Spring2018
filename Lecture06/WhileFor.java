public class WhileFor
{
    public static void main(String[] args)
    {
        
        //criteria for a counter controlled loop:
        //1. a counter variable that acts as the loop control variable
        //2. check the variable for the value by which you want to stop
        //3. update the loop control variable
        
        //while loop version of a counter controlled loop
        int counter = 0;                            //declaring loop control variable
        while (counter < 10)                        //evaulation of looping action
        {
            System.out.printf("%d ", counter);
            counter = counter + 1;                  //update the variable
        }
        System.out.print("\n");
        
        //for loop version of a counter controlled loop
        for (int count=0; count<10; count=count+1)
        {
            System.out.printf("%d ", count);
        }
        System.out.print("\n");
        
        //for loop as while loop structure
        
        //declare variable
        int count =0;
        //evaluate the variable
        for(;count<10;)
        {
            System.out.printf("%d ", count);
            count = count + 1; //update the variable
        }
        System.out.print("\n");
        
    }
}