public class WhileLoop
{
    
    //1. You MUST create a loop control variable that controls the loop
    //2. You evaluate the loop control variable
    //3. You must update the loop control variable such that it becomes false to end the loop
    
    public static void main(String[] args)
    {
        int product = 300;
        
        while (product < 100)
        {
            System.out.printf("The product is: %d\n", product);
            product = product * 3;
        }
        
        System.out.printf("The product is: %d\n", product);
        
    }
}