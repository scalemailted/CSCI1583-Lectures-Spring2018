public class CompoundAssignment
{
    public static void main(String[] args)
    {
        //compound assignment with arithmetic opereations
        int x = 2;
        x *= 10;
        System.out.printf("result: %d\n", x);
        
        //compound assignment with concatenation operations
        String text = "Hello";
        text += "World";
        System.out.printf("result: %s\n", text);
        
        //pre increment
        x = 0;
        System.out.printf("before preincrement: %d\n", x);
        System.out.printf("during preincrement: %d\n", ++x);
        System.out.printf("after preincrement: %d\n", x);
        
        x = 0;
        //post increment
        System.out.printf("before postincrement: %d\n", x);
        System.out.printf("during postincrement: %d\n", x++);
        System.out.printf("after postincrement: %d\n", x);
    }
}