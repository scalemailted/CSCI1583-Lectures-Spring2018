public class DoWhile
{
    public static void main(String[] args)
    {
        //while loop
        System.out.print("Before the while loop!\n");
        int i = 10;
        while (i<10)
        {
            System.out.printf("%d ", i);
            i = i + 1;
        }
        System.out.print("\nAfter the while loop\n");
        
        //do while loop
        System.out.print("Before the do while loop!\n");
        int j = 11;
        do
        {
            System.out.printf("%d ", j); //code goes here
            j = j + 1;
        }while( j< 10);
        System.out.print("\nAfter the do while loop\n");
        
        //common mistake
        int x = 1;
        while (x < 10);
        {
            System.out.printf("%d ", x);
            x = x + 1;
        }
        System.out.print("\n");
    }
}