public class D6x2Tester
{
    public static void main(String[] args)
    {
        int[] frequency = new int[19];
        for (int i=0; i<11000000; i++)
        {
            int diceSum = Dice.rollD6(3);
            frequency[diceSum]++;
        }
        
        for (int diceFace=3; diceFace<frequency.length; diceFace++)
        {
            System.out.printf("%d: %d\n", diceFace, frequency[diceFace] );
        }
    }
}