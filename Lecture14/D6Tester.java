public class D6Tester
{
    public static void main(String[] args)
    {
        int result = Dice.rollD6();
        System.out.printf("invoked Dice.rollD6() -> %d\n\n", result);
        
        System.out.println("For Loop:");
        for(int i=0; i<20; i++)
        {
            result = Dice.rollD6(i);
            System.out.printf("invoked Dice.rollD6(%d) -> %d\n\n", i, result);
        }
    }
}