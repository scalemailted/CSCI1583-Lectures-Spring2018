public class EnhancedFor
{
    public static void main(String[] args)
    {
        String[] namesArray = {"Tom", "Mary", "Sue", "John"};
        
        //Traditional For loop for reading
        for(int i=0; i<namesArray.length; i++)
        {
            String name = namesArray[i];
            System.out.printf("%s\t", name);
        }
        System.out.print("\n");
        
        //Enhanced FOR loop for writing
        for (String name : namesArray)
        {
            name = "YouveBeenHaxed!";
            System.out.printf("%s\t", name);
        }
        System.out.print("\n");
        
         //Traditional For loop for writing
        for(int i=0; i<namesArray.length; i++)
        {
            namesArray[i] = "YouveBeenHaxed!";
            String name = namesArray[i];
            System.out.printf("%s\t", name);
        }
        System.out.print("\n");
        
         //Enhanced FOR loop for reading
        for (String name : namesArray)
        {
            System.out.printf("%s\t", name);
        }
        System.out.print("\n");
        
        //Enhanced FOR loop with writing
        int i = 0;
        for (String name : namesArray)
        {
            namesArray[i] = "Ted";
            System.out.printf("%s\t", name);
            i++;
        }
        System.out.print("\n");
    }
}