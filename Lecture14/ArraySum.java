public class ArraySum
{
    public static void main(String[] args)
    {
        int[] grades = {23, 43, 34, 56, 100};
        int sum = 0;
        
        for (int grade : grades)
        {
            sum += grade;
        }
        System.out.printf("The average is: %d\n", sum/grades.length );
    }
}