public class ATM
{
    public static void main(String[] args)
    {
        Account account1 = new Account();
        System.out.printf("New Account Balance: $%.2f\n", account1.getBalance() );
        account1.balance = 10000000;
        System.out.printf("Account Balance because public: $%.2f\n", account1.getBalance() );
    }
}