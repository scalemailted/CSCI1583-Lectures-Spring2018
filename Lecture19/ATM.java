import java.util.ArrayList;
import java.util.Scanner;

public class ATM
{
    private ArrayList<Account> accounts;
    private Account loggedinAccount;
    private Scanner input;
    
    public ATM()
    {
        this.accounts = new ArrayList<Account>();
        this.input = new Scanner(System.in);
        
    }
    
    public static void main(String[] args)
    {
        ATM atm = new ATM();
        boolean isDone = false;
        while (isDone == false)
        {
            atm.displayMainMenu();
            String choice = atm.input.nextLine();
            if (choice.equals("1"))
            {
                //make a new account
                atm.addAccount();
            }
            else if (choice.equals("2"))
            {
                //display accounts
                atm.displayAccounts();
            }
            else if (choice.equals("3") )
            {
                isDone = true;
            }
        }
    }
    
    
    public void displayMainMenu()
    {
        System.out.print("1. Make a new Account\n");
        System.out.print("2. Access an existing Account\n");
        System.out.print("3. Quit\n");
    }
    
    public void addAccount()
    {
        System.out.print("Enter your full name: ");
        String name = input.nextLine();
        System.out.print("Enter a 4 digit PIN: ");
        String pin = input.next();
        Account account = new Account(name, pin);
        accounts.add(account);
        input.nextLine();
        System.out.println();
    }
    
    public void displayAccounts()
    {
        if (accounts.size() == 0)
        {
            System.out.println("There are no accounts.");
        }
        
        System.out.print("\nAccounts:\n");
        System.out.print("===========\n");
        for (Account account : accounts)
        {
            System.out.printf("%d: %s\n",account.getID(), account.getName() );
        }
        System.out.println();
        System.out.print("Enter an account id to access: ");
        int id = atm.input.nextInt();
        atm.input.nextLine();
        System.out.print("Enter a PIN number for that account: ");
        String pin = atm.input.next();
        for (Account account: accounts)
        {
            if (id == account.getID() )
            {
                if (account.equalPin(pin) )
                {
                    loggedinAccount = account;
                }
                else
                {
                    System.out.println("Invalid PIN");
                }
            }
        }
        
        
        
        
    }
}