import java.util.Scanner;

public class DRYAverager
{
    public static void main(String[] args)
    {
        double testAverage = getAverage("Test");
        double hwAverage =  getAverage("Homework");
        double labAverage = getAverage("Lab");
        double finalAverage = testAverage*0.4 + labAverage*0.1 + hwAverage*0.5;
        System.out.printf("Final Grade: %.2f\n", finalAverage);
    

    }
    
    public static double getAverage (String gradeType)
    {
        // Initialize sum to 0
        double sum = 0;
        // Initialize count to 0
        int count = 0;
        // Initialize Scanner 
        Scanner input = new Scanner(System.in);
        
        System.out.printf("Enter %s grades or -1 to quit: ", gradeType);
        double grade = input.nextDouble();
        
        // Repeat until the grade is less than 0
        while (grade >= 0)
        {
            // add grade to sum
            sum += grade;
            // add 1 to count
            count++;
            grade = input.nextDouble();
        }
        // Calculate Lab average 
        double average = sum / count;
        System.out.printf("%s Average is %.2f\n",gradeType, average);
        return  average;
    }
}