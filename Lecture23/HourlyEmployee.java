public class HourlyEmployee extends Employee
{
    private double hourlyRate;
    private double hoursWorked;
    
    public HourlyEmployee(String name, Date hire, double rate, double worked)
    {
        super(name, hire);
        this.hourlyRate = rate;
        this.hoursWorked = worked;
    }
    
    public double getPayment()
    {
        return this.hourlyRate * this.hoursWorked ;
    }
    
    public String toString()
    {
        String employee = super.toString();
        return String.format("%s, hourly:$%.02f @ %.02f hours", employee, this.hourlyRate, this.hoursWorked);
    }
}