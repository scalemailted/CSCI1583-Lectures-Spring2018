public class Employee
{
    private String name;
    private Date hireDate;
    
    public Employee(String name, Date hire)
    {
        this.name = name;
        this.hireDate = hire;
    }
    
    public Date getHireDate()
    {
        return this.hireDate;
    }
    
    public String toString()
    {
        return String.format("%s, hired: %s", this.name, this.hireDate);
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public double getPayment()
    {
        return 0;
    }
    
}