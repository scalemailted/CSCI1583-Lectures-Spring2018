public class CommisionEmployee extends Employee
{
    private double rate;
    private double sales;
    
    public CommisionEmployee(String name, Date hire, double rate, double sales)
    {
        super(name, hire);
        this.rate = rate;
        this.sales = sales;
    }
    
    
    public String toString()
    {
        String employee = super.toString();
        return String.format("%s, commission:%.02f%% @ $%.02f sales", employee, this.rate, this.sales);
    }
}