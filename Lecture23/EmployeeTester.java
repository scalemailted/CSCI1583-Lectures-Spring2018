public class EmployeeTester
{
    public static void main(String[] args)
    {

        HourlyEmployee tim = new HourlyEmployee("Tim Green", new Date(15,2,2012),2.25,4 );
        //System.out.println(tim.toString());
        //System.out.printf("Tims's Pay period amomunt: $%.02f\n", tim.getPayment() );
        SalaryEmployee sue = new SalaryEmployee("Sue Suzie", new Date(21,1,2000), 100000 );
        //System.out.println(sue);
        //System.out.printf("Sue's Pay period amomunt: $%.02f\n", sue.getPayment() );
        
        /*Object[] arr = {1, 1.0, true, "cat", new Date(21,1,2000), tim, sue, 'a'};
        for (Object item : arr)
        {
            System.out.println(item);
        }*/
        
        Employee[] employees = {tim, sue};
        for (Employee worker : employees)
        {
            //System.out.println(worker);
            System.out.printf("%s's Payment amomunt: $%.02f\n", worker.getName() , worker.getPayment() );
        }
    }
}