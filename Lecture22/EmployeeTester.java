public class EmployeeTester
{
    public static void main(String[] args)
    {

        Employee tim = new Employee("Tim Green", new Date(15,2,2012) );
        System.out.println(tim);
        SalaryEmployee sue = new SalaryEmployee("Sue Suzie", new Date(21,1,2000), 100000 );
        System.out.println(sue);
    }
}