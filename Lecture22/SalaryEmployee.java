public class SalaryEmployee extends Employee
{
    private double salary;
    
    public SalaryEmployee(String name, Date hire, double salary)
    {
        super(name, hire);
        this.salary = salary;
    }
    
    public double getSalary()
    {
        return this.salary;
    }
    
    public String toString()
    {
        String employee = super.toString();
        return String.format("%s, salary:$%.02f", employee, this.salary);
    }
}