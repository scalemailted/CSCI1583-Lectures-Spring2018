public class EmployeeTester
{
    public static void main(String[] args)
    {
        try
        {
            Employee tim = new Employee("Tim Green", new Date(15,2,-2012) );
            System.out.println(tim);
        }
        catch (IllegalArgumentException e)
        {
            System.out.println(e);
            Employee tim = new Employee("Tim Green", new Date(1,1,2018) );
            System.out.println(tim);
        }
    }
}