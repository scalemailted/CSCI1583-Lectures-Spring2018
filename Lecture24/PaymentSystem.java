public class PaymentSystem
{
    public static void main(String[] args)
    {

        HourlyEmployee tim = new HourlyEmployee("Tim Green", new Date(15,2,2012),2.25,4 );
        SalaryEmployee sue = new SalaryEmployee("Sue Suzie", new Date(21,1,2000), 100000 );
        CommissionEmployee ted = new CommissionEmployee("Ted Ted", new Date(05, 7, 1900), 0.25, 200 );
        Invoice invoice1 = new Invoice(300, "Mops & Brooms", "A34-6C");
        
        Payable[] costlyThings = {tim, sue, ted, invoice1};
        for (Payable i : costlyThings)
        {
            printEmployeePaycheck(i);
            
        }
    }
    
    public static void printEmployeePaycheck(Payable costly)
    {
        System.out.printf("%s: $%.02f\n", costly , costly.getCost() );
    }
}