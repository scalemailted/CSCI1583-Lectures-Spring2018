public class SalaryEmployee extends Employee
{
    private double salary;
    
    public SalaryEmployee(String name, Date hire, double salary)
    {
        super(name, hire);
        this.salary = salary;
    }
    
    public double getPayment()
    {
        return (this.salary / 12) / 2;
    }
    
    public String toString()
    {
        String employee = super.toString();
        return String.format("%s, salary:$%.02f", employee, this.salary);
    }
}