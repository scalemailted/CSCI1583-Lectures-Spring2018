public class CommissionEmployee extends Employee
{
    private double rate;
    private double sales;
    
    public CommissionEmployee(String name, Date hire, double rate, double sales)
    {
        super(name, hire);
        this.rate = rate;
        this.sales = sales;
    }
    
    public double getPayment()
    {
        return rate * sales;
    }
    
    
    public String toString()
    {
        String employee = super.toString();
        return String.format("%s, commission:%.02f%% @ $%.02f sales", employee, this.rate, this.sales);
    }
}