public class EmployeeTester
{
    public static void main(String[] args)
    {

        HourlyEmployee tim = new HourlyEmployee("Tim Green", new Date(15,2,2012),2.25,4 );
        SalaryEmployee sue = new SalaryEmployee("Sue Suzie", new Date(21,1,2000), 100000 );
        CommissionEmployee ted = new CommissionEmployee("Ted Ted", new Date(05, 7, 1900), 0.25, 200 );
        
        Employee[] employees = {tim, sue, ted};
        for (Employee worker : employees)
        {
            printEmployeePaycheck(worker);
            
        }
    }
    
    public static void printEmployeePaycheck(Employee worker)
    {
        System.out.printf("%s's Payment amomunt: $%.02f\n", worker.getName() , worker.getPayment() );
    }
}