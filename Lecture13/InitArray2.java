public class InitArray2
{
    public static void main(String[] args)
    {
        String[] groceryArray = {"eggs", "milk", "bread", "coldcuts", "cookies", "ice cream"};
        
        for (int i=0; i<groceryArray.length; i++ )
        {
            String item = groceryArray[i];
            System.out.printf("%d: %s\n",i,item);
        }
    }
}