public class InitArray
{
    public static void main(String[] args)
    {
        String[] groceryArray = new String[6];
        groceryArray[0] = "eggs";
        groceryArray[1] = "milk";
        groceryArray[2] = "bread";
        groceryArray[3] = "coldcuts";
        groceryArray[4] = "cookies";
        //groceryArray[5] = "ice cream";
        
        for (int i=0; i<groceryArray.length; i++ )
        {
            String item = groceryArray[i];
            System.out.printf("%d: %s\n",i,item);
        }
    }
}