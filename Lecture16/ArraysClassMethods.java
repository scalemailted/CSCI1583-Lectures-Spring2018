import java.util.Arrays;

public class ArraysClassMethods
{
    public static void main(String[] args)
    {
        int[] array = {2,4,3,5,1};
        System.out.println(array);
        System.out.println( Arrays.toString(array) );
        printArray(array);
        
        //Sort
        Arrays.sort(array);
        System.out.println("Sorted Array");
        System.out.println( Arrays.toString(array) );
        
        //Fill Array
        int[] fillArray = new int[20];
        Arrays.fill(fillArray, 7);
        System.out.println( Arrays.toString(fillArray) );
        
        //Equality
        int[] a ={1,2,4};
        int[] b = {1,2,4};
        boolean bool = a == b;
        System.out.println("Equality Test with ==");
        System.out.printf("primitive equality: %b\n", bool);
        bool = Arrays.equals(a,b);
        System.out.printf("array eqaulity: %b\n", bool);
        
        //search
        System.out.println( Arrays.toString(array) );
        int location = Arrays.binarySearch(array, 3);
        System.out.printf("The location of 3 is in the index of %d\n", location);
        location = Arrays.binarySearch(array, 6);
        System.out.printf("The location of 6 is in the index of %d\n", location);
        
        
    }
    
    //Our version of a toString method
    public static void printArray(int[] arr)
    {
        String arrText = "[";
        for (int i : arr)
        {
            arrText += i + " ";
        }
        arrText += "]";
        System.out.println(arrText);
    }
}