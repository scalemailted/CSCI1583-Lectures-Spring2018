public class StudentPoll
{
    public static void main(String[] args)
    {
        int[] responses = {1,3,2,2,5,3,1,2,4,5,4,3,2,3,35};
        
        int[] frequency = new int[6];
        
        for(int score : responses)
        {
            try
            {
                frequency[score]++;
            }
            catch(ArrayIndexOutOfBoundsException e)
            {
                System.out.println(e);
            }
        }
        
        for (int i=1; i<frequency.length; i++)
        {
            System.out.printf("%d: %d\n", i, frequency[i]);
        }
    }
}