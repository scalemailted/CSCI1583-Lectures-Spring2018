public class CommandArgs
{
    public static void main(String[] args)
    {
        System.out.printf("The length of args is %d\n", args.length);
        if (args.length > 0)
        {
            for (String str : args)
            {
                System.out.println(str);
            }
        }
    }
}