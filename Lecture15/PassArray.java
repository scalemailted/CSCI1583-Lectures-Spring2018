public class PassArray
{
    public static void main(String[] args)
    {
        int[] intArray = {73, 87, 57, 100, 2};
        printArray(intArray);
        changeElement(intArray[0]);
        printArray(intArray);
        changeArray(intArray);
        printArray(intArray);
    }
    
    public static void changeElement(int element)
    {
        System.out.printf("%d changed to 0\n", element);
        element = 0;
    }
    
    public static void changeArray(int[] array)
    {
        for (int i=0; i<array.length; i++)
        {
            array[i] *= 10;
        }
    }
    
    public static void printArray(int[] array)
    {
        String arrString = "{ ";
        for (int number : array)
        {
            arrString += number + " ";
        }
        arrString += "}";
        System.out.println(arrString);
    }
}